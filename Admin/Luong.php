<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Quản lý Lương - Admin</title>

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  <script src="vendor/jquery-3.3.1.min.js"></script>

    <link rel="stylesheet" href="vendor/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/bootstrap.min.css.map">

    <script src="vendor/gijgo.min.js" type="text/javascript"></script>
	  <link href="vendor/gijgo.min.css" rel="stylesheet" type="text/css" />
	<style>
		.input-group-append{
			display:none;
		}

	</style>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

  <?php include("Share/sidebar.php");?>


    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

      <?php include("Share/topnav.php");?>



        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Quản lý Lương</h1>
			<!-- <a class="dropdown-item" id="add_button" href="#" data-toggle="modal" data-target="#themluong">
				<i class="fas fa-plus fa-sm fa-fw mr-2 text-gray-400"></i>
				Thêm mới Lương
			</a> -->
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Danh sách Lương</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="bangluong" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Mã nhân viên</th>
                      <th>Tên</th>
                      <th>Chức vụ</th>
                      <th>Lương cơ bản</th>
                      <th>Hệ số lương</th>
                      <th>Ghi chú</th>
					  <th>Lương</th>
					  <th>Sửa</th>

                    </tr>
                  </thead>                
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2020</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="themluong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="min-width:800px" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
			<form method="POST" id="luong_form">
        <div class="row">          
          <div class="form-group col-md-4">
            <label for="ten_add" class="col-form-label">Tên:</label>
            <input type="text" disabled name="ten_add" class="form-control" id="ten_add">
          </div>
          <div class="form-group col-md-4">
            <label for="chucvu_add" class="col-form-label">Chức vụ</label>
            <input type="text" disabled name="chucvu_add" class="form-control" id="chucvu_add">
          </div>
          <div class="form-group col-md-4">
            <label for="phongban_add" class="col-form-label">Phòng ban</label>
            <input type="text" disabled name="phongban_add" class="form-control" id="phongban_add">
          </div>
		  <div class="form-group col-md-4">
            <label for="luongcoban" class="col-form-label">Lương cơ bản</label>
            <input type="text" name="luongcoban" class="form-control" id="luongcoban">
          </div>
		  <div class="form-group col-md-4">
            <label for="hesoluong" class="col-form-label">Hệ số lương</label>
            <input type="text" name="hesoluong" class="form-control" id="hesoluong">
          </div>
		  <div class="form-group col-md-12">
		  <label class="col-form-label">Ghi chú</label>
			<div class="row">
				<div class="col-md-6" style="border-right: 2px solid;">
					<div class="row">
						<div class="col-md-6">
							<label for="phucap_ghichu" class="col-form-label">Phụ cấp</label>
							<input type="text" name="phucap_ghichu" class="form-control" id="phucap_ghichu">
						</div>
						<div class="col-md-6">
							<label for="phucap_tien" class="col-form-label">Số tiền</label>
							<input type="text" name="phucap_tien" class="form-control" id="phucap_tien">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-6">
							<label for="phat_ghichu" class="col-form-label">Phạt</label>
							<input type="text" name="phat_ghichu" class="form-control" id="phat_ghichu">
						</div>
						<div class="col-md-6">
							<label for="phat_tien" class="col-form-label">Số tiền</label>
							<input type="text" name="phat_tien" class="form-control" id="phat_tien">
						</div>
					</div>
				</div>		
			</div>            
          </div>
		 
        </div>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="luong_id" id="luong_id" />
					<input type="hidden" name="operation" id="operation" />
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Hủy</button>
					<input type="submit" name="action" id="action" class="btn btn-success" value="Add" />
        </div>
      </div>
    </div>
		</form>
  </div>

  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Bạn có chắc chắn muốn đăng xuất</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Chọn Đăng xuất để thoát ra khỏi hệ thống</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Hủy</button>
          <a class="btn btn-primary" href="/bai3/logout.php">Đăng xuất</a>
        </div>
      </div>
    </div>
  </div>
</div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>

  <script type="text/javascript" language="javascript" > 
	
	$(document).ready(function(){
		// $('#add_button').click(function(){
		// 	$('#luong_form')[0].reset();
		// 	$('.modal-title').text("Thêm nhân viên");
		// 	$('#action').val("Thêm");
		// 	$('#operation').val("Add");
		// });
		
		var dataTable = $('#bangluong').DataTable({
			"processing":true,
			"serverSide":true,
			"order":[],
			"ajax":{
				url:"Model/luong/fetch.php",
				type:"POST"
			},
			"columnDefs":[
				{
					"targets":[7],
					"orderable":false,
				},
			],

		});

		$(document).on('click', '.update', function(){
			var luong_id = $(this).attr("id");
			$.ajax({
				url:"Model/luong/fetch_single.php",
				method:"POST",
				data:{luong_id:luong_id},
				dataType:"json",
				success:function(data)
				{
					$('#themluong').modal('show');
          $('#luong_id').val(luong_id);
          console.log($('#luong_id').val());
          $('.modal-title').text("Sửa Lương nhân viên");
					$('#ten_add').val(data.Ten);          
          $('#chucvu_add').val(data.ChucVu);
          $('#phongban_add').val(data.PhongBan);
          $('#luongcoban').val(data.luongcoban);
          $('#phongban_add').val(data.PhongBan);
          $('#phongban_add').val(data.PhongBan);

					$('#action').val("Sửa");
					$('#operation').val("Edit");
				}
			})
		});

		$(document).on('submit', '#luong_form', function(event){
			event.preventDefault();
			var dateStart_add = $('#dateStart_add').val();
			var dateEnd_add = $('#dateEnd_add').val();

			if(dateStart_add!=''&& dateEnd_add!='')
			{
				$.ajax({
					url:"Model/luong/insert.php",
					method:'POST',
					data:new FormData(this),
					contentType:false,
					processData:false,
					success:function(data)
					{
						alert(data);
            // console.log(data);
						$('#luong_form')[0].reset();
						$('#themluong').modal('hide');
						dataTable.ajax.reload();
					}
				});
			}
			else
			{
				alert("Không được để trống");
			}
		});
	});
</script>
</body>

</html>
